import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  msgFromServe: Observable<any>;

  constructor(private https: HttpClient) {
    this.getDataFromBackend();
  }

  getDataFromBackend() {
    this.msgFromServe = this.https.get('http://localhost:3000/');
  }
}
